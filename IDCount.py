import os
import openpyxl
from time import strftime

idranfiles = []
list_39to39_wmpttoumpt = []
list_newones = []
list_39to59 = []

for dirpath, dirnames, filenames in os.walk("IDRANs"):
    for filename in filenames:
        if filename.split('.')[-1] == 'xlsx':
            idranfiles.append(str(''.join(list(os.path.join(dirpath, filename)))))
if len(idranfiles) == 0:
    print('\n', strftime(" %H:%M:%S"), 'No IDRAN files found in "IDRANs" folder.\n Operation failed.')
    input("Press Enter to exit...")
    raise SystemExit

qty = len(idranfiles)
filenum = 0
print(strftime(" %H:%M:%S"), 'Data processing started...')
for file in idranfiles:
    filenum += 1
    print(strftime(" %H:%M:%S"), f'[{filenum}/{qty}]', 'File "' + file + '" is parsing...                   ', end="\r")
    try:
        book = openpyxl.open(file, read_only=True)
        sheet = book['CPRI']
    except:
        pass
    else:
        # разбор открытого IDRAN
        rowid = 1
        for row in sheet.iter_rows(values_only=True):
            rowid += 1
            try:
                if row[6][:5] == 'BBU39' and row[2][:5] == 'BBU39':
                    if ((sheet.cell(row=rowid+3, column=2).value[:4] == 'WMPT'
                         or sheet.cell(row=rowid+4, column=2).value[:4] == 'WMPT')
                        and (sheet.cell(row=rowid+3, column=6).value[:4] == 'UMPT'
                             or sheet.cell(row=rowid+4, column=6).value[:4] == 'UMPT')):
                        sheet = book.worksheets[0]
                        list_39to39_wmpttoumpt.append([file.split('\\')[1],
                                                       sheet.cell(row=2, column=3).value,
                                                       sheet.cell(row=4, column=3).value])
            except:
                pass
            try:
                if row[6][:5] in ['BBU39', 'BBU59'] and row[2] is None:
                    sheet = book.worksheets[0]
                    list_newones.append([file.split('\\')[1],
                                         sheet.cell(row=2, column=3).value,
                                         sheet.cell(row=4, column=3).value])
            except:
                pass
            try:
                if row[6][:5] == 'BBU59' and row[2][:5] == 'BBU39':
                    sheet = book.worksheets[0]
                    list_39to59.append([file.split('\\')[1],
                                        sheet.cell(row=2, column=3).value,
                                        sheet.cell(row=4, column=3).value])
            except:
                pass

        book.close()

book = openpyxl.Workbook()

sheet = book.create_sheet('3900+WMPT to 3900+UMPT')
alist = []
rowid = 0
for bs in list_39to39_wmpttoumpt:
    if bs not in alist:
        alist.append(bs)
        rowid += 1
        sheet.cell(row=rowid, column=1).value = bs[0]
        sheet.cell(row=rowid, column=2).value = bs[1]
        sheet.cell(row=rowid, column=3).value = bs[2]

sheet = book.create_sheet('New BS')
alist = []
rowid = 0
for bs in list_newones:
    if bs not in alist:
        alist.append(bs)
        rowid += 1
        sheet.cell(row=rowid, column=1).value = bs[0]
        sheet.cell(row=rowid, column=2).value = bs[1]
        sheet.cell(row=rowid, column=3).value = bs[2]


sheet = book.create_sheet('3900 series to 5900 series')
alist = []
rowid = 0
for bs in list_39to59:
    if bs not in alist:
        alist.append(bs)
        rowid += 1
        sheet.cell(row=rowid, column=1).value = bs[0]
        sheet.cell(row=rowid, column=2).value = bs[1]
        sheet.cell(row=rowid, column=3).value = bs[2]

book.save('result.xlsx')

print('\n\n Processing complete.\n See statistics:\n')
print('IDRANs in total:', len(idranfiles))
print('\n3900 series with WMPT to 3900 series with UMPT:', len(list_39to39_wmpttoumpt))
for bs in list_39to39_wmpttoumpt:
    print(bs)
print('\nNew BS:', len(list_newones))
for bs in list_newones:
    print(bs)
print('\n3900 series to 5900 series:', len(list_39to59))
for bs in list_39to59:
    print(bs)

input("Press Enter to exit...")
raise SystemExit
